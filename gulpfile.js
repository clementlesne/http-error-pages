'use strict';

const
    fs = require('fs'),
    gulp = require('gulp'),
    glob = require('glob'),
    pug = require('gulp-pug'),
    bump = require('gulp-bump'),
    gutil = require('gulp-util'),
    uncss = require('gulp-uncss'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    cleanCss = require('clean-css'),
    merge = require('merge-stream'),
    htmlmin = require('gulp-htmlmin'),
    compass = require('gulp-compass'),
    replace = require('gulp-replace'),
    purifyCss = require('purify-css'),
    responsive = require('gulp-responsive'),
    gulpCleanCSS = require('gulp-clean-css'),
    replaceTask = require('gulp-replace-task'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();

var paths = {
    templates__test: 'app/views/**/*.pug',
    templates_pages__test: 'app/views/pages/*.pug',
    templates_html__test: 'dist/**/*.html',
    templates_html__dist: 'dist',
    css__test: 'app/css/**/*.css',
    css__dist: 'dist/css',
    css__dist_filename: 'app.css',
    sass: 'app/scss',
    sass__test: 'app/scss/**/*.scss',
    sass__dist: '.tmp/css',
    other_root__test: 'app/*',
    other_root__dist: 'dist',
    all_test: 'dist/**/*',
    all_dist: 'dist'
};

var getPackageJson = function () {
    return JSON.parse(fs.readFileSync('package.json', 'utf8'));
};

// Static server
gulp.task('serve', ['compile'], function() {
    browserSync
        .init({
            server: {
                baseDir: 'dist'
            }
        });

    gulp.watch(paths.templates__test, ['bump', 'templates__compile']);
    gulp.watch([paths.css__test, paths.sass__test], ['bump', 'css__compile']);
    gulp.watch(paths.other_root__test, ['bump', 'other']);
});

gulp.task('compile', ['templates__compile', 'css__compile', 'other']);

gulp.task('compile-optimized', ['compile'], function() {
    fs.readFile(paths.css__dist + '/' + paths.css__dist_filename, 'utf8', function ($err, $cssRaw) {
        if ($err) return gutil.log(gutil.colors.red($err));

        $cssRaw = $cssRaw.replace(/\/\*.*\*\//, '');

        glob(paths.templates_html__test, function ($err, $files) {
            if ($err) return gutil.log(gutil.colors.red($err));

            $files.forEach(function ($file) {
                gutil.log('optimizing file:', $file);

                fs.readFile($file, 'utf8', function ($err, $fileContent) {
                    purifyCss($fileContent, $cssRaw, {
                        minify: false,
                        output: false,
                        info: false,
                        rejected: false
                    }, function ($cssCritical) {
                        new cleanCss({
                            inline: false,
                            returnPromise: true,
                            level: {
                                1: {
                                    all: true,
                                    normalizeUrls: false,
                                    transform: function (propertyName, propertyValue) {
                                        if (propertyValue.indexOf('../') > -1) {
                                            return propertyValue.replace('../', '');
                                        }
                                    }
                                },
                                2: {
                                    restructureRules: true
                                }
                            }
                        })
                            .minify($cssCritical)
                            .then(function ($cssMinified) {
                                var $data = $fileContent.replace(/<!--!@@css-critical-->/g, '<style>' + $cssMinified.styles + '</style>');
                                fs.writeFile($file, $data, 'utf8', function ($err) {
                                    if ($err) return gutil.log(gutil.colors.red($err));
                                });
                            });
                    });
                });
            });
        });
    });
});

gulp.task('bump', ['bump__code']);

gulp.task('bump__code', function(){
    gulp.src('package.json')
        .pipe(bump({
            type:'prerelease'
        }))
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); })
        .pipe(gulp.dest('./'));
});

gulp.task('css__compile', function () {
    var sassStream,
        cssStream;

    sassStream = gulp.src(paths.sass__test)
        .pipe(compass({
            project: __dirname,
            css: paths.sass__dist,
            sass: paths.sass,
            image: paths.images
        }))
        .pipe(uncss({
            html: [paths.templates_html__test],
            ignore: [new RegExp('amp*'), new RegExp('wp__*'), '.hidden']
        }))
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); });

    cssStream = gulp.src(paths.css__test)
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); });

    return merge(cssStream, sassStream)
        .pipe(autoprefixer({
            browsers: ['> 3%', 'IE 8'],
            cascade: true
        }))
        .pipe(concat(paths.css__dist_filename))
        .pipe(replace('@charset "UTF-8";', ''))
        .pipe(replaceTask({
            patterns: [
                {
                    match: 'version',
                    replacement: getPackageJson().version
                }
            ]
        }))
        .pipe(gulpCleanCSS({
            compatibility: 'ie9',
            advanced: true,
            roundingPrecision: 3
        }))
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); })
        .pipe(gulp.dest(paths.css__dist));
});

gulp.task('templates__compile', function () {
    return gulp.src(paths.templates_pages__test)
        .pipe(pug())
        .pipe(htmlmin({
            minifyURLs: true,
            sortAttributes: true,
            sortClassName: true,
            useShortDoctype: true,
            removeAttributeQuotes: true,
            quoteCharacter: '\"',
            minifyJS: true
        }))
        .pipe(replaceTask({
            patterns: [
                {
                    match: 'version',
                    replacement: getPackageJson().version
                }
            ]
        }))
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); })
        .pipe(gulp.dest(paths.templates_html__dist));
});

gulp.task('other', ['other_root__copy']);

gulp.task('other_root__copy', function () {
    gulp.src(paths.other_root__test)
        .pipe(replaceTask({
            patterns: [
                {
                    match: 'version',
                    replacement: getPackageJson().version
                },
                {
                    match: 'timestamp',
                    replacement: new Date().getTime()
                }
            ]
        }))
        .on('error', function($err) { gutil.log(gutil.colors.red($err.message)); })
        .pipe(gulp.dest(paths.other_root__dist));
});